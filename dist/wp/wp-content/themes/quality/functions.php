<?php
/* SVGアップロード */
add_filter('upload_mimes', function($mimes){
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
});

/* 管理画面メニュー */
add_action('admin_menu', function(){
	remove_menu_page('edit.php');
	remove_menu_page('edit.php?post_type=page');
	remove_menu_page('edit-comments.php');
});

/*
 * カスタム投稿：派遣依頼案件
 */
add_action('init', function(){
  register_post_type('work', [
    'label' => '派遣依頼案件',
    'public' => true,
    'menu_position' => 5,
  ]);
  register_taxonomy('workcat', 'work', [
    'label' => 'カテゴリー',
    'hierarchical' => true,
  ]);
});
